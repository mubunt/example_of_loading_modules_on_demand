 # *example_of_loading_modules_on_demand*, Example of loading modules on demand.

Example of how to use shared library and dynamic loading. Some URL on that:
- [Dynamically Loaded (DL) Libraries](https://tldp.org/HOWTO/Program-Library-HOWTO/dl-libraries.html)
- [Shared libraries with GCC on Linux](https://www.cprogramming.com/tutorial/shared-libraries-linux-gcc.html)
- [Building And Using Static And Shared "C" Libraries](https://docencia.ac.upc.edu/FIB/USO/Bibliografia/unix-c-libraries.html)

## LICENSE
**example_of_loading_modules_on_demand** is covered by the GNU General Public License (GPL) version 3 and above.

## USAGE
``` bash
$ ./linux/example_of_loading_modules_on_demand 
USAGE: ./linux/example_of_loading_modules_on_demand [ acq ] [ dec ] [ col ] [ enc ] [ dis ]
$ ./linux/example_of_loading_modules_on_demand acq dec col enc dis
Loading library /home/michel/Sandbox/GitLab/example_of_loading_modules_on_demand/linux/libacq.so
Executing entry point
Module ACQ ... OK
Closing library /home/michel/Sandbox/GitLab/example_of_loading_modules_on_demand/linux/libacq.so
Loading library /home/michel/Sandbox/GitLab/example_of_loading_modules_on_demand/linux/libdec.so
Executing entry point
Module DEC ... OK
Closing library /home/michel/Sandbox/GitLab/example_of_loading_modules_on_demand/linux/libdec.so
Loading library /home/michel/Sandbox/GitLab/example_of_loading_modules_on_demand/linux/libcol.so
Executing entry point
Module COL ... OK
Closing library /home/michel/Sandbox/GitLab/example_of_loading_modules_on_demand/linux/libcol.so
Loading library /home/michel/Sandbox/GitLab/example_of_loading_modules_on_demand/linux/libenc.so
Executing entry point
Module ENC ... OK
Closing library /home/michel/Sandbox/GitLab/example_of_loading_modules_on_demand/linux/libenc.so
Loading library /home/michel/Sandbox/GitLab/example_of_loading_modules_on_demand/linux/libdis.so
Executing entry point
Module DIS ... OK
Closing library /home/michel/Sandbox/GitLab/example_of_loading_modules_on_demand/linux/libdis.so
$ ./linux/example_of_loading_modules_on_demand acq enc
Loading library /home/michel/Sandbox/GitLab/example_of_loading_modules_on_demand/linux/libacq.so
Executing entry point
Module ACQ ... OK
Closing library /home/michel/Sandbox/GitLab/example_of_loading_modules_on_demand/linux/libacq.so
Loading library /home/michel/Sandbox/GitLab/example_of_loading_modules_on_demand/linux/libenc.so
Executing entry point
Module ENC ... OK
Closing library /home/michel/Sandbox/GitLab/example_of_loading_modules_on_demand/linux/libenc.so
$ 

```
## STRUCTURE OF THE APPLICATION
This section walks you through **example_of_loading_modules_on_demand**'s structure. Once you understand this structure, you will easily find your way around in **example_of_loading_modules_on_demand**'s code base.

``` bash
$ yaTree
./                                             # Application level
├── libacq/                                    # ACQ source directory (dynamical loaded file)
│   ├── Makefile                               # Makefile
│   └── acq.c                                  # Dynamical loaded example
├── libcol/                                    # COL source directory (dynamical loaded file)
│   ├── Makefile                               # Makefile
│   └── col.c                                  # Dynamical loaded example
├── libdec/                                    # DEC source directory (dynamical loaded file)
│   ├── Makefile                               # Makefile
│   └── dec.c                                  # Dynamical loaded example
├── libdis/                                    # DIS source directory (dynamical loaded file)
│   ├── Makefile                               # Makefile
│   └── dis.c                                  # Dynamical loaded example
├── libenc/                                    # ENC source directory (dynamical loaded file)
│   ├── Makefile                               # Makefile
│   └── enc.c                                  # Dynamical loaded example
├── src/                                       # Source directory
│   ├── Makefile                               # Makefile
│   └── example_of_loading_modules_on_demand.c # Source of example
├── COPYING.md                                 # GNU General Public License markdown file
├── LICENSE.md                                 # License markdown file
├── Makefile                                   # Makefile
├── README.md                                  # ReadMe markdown file
├── RELEASENOTES.md                            # Release Notes markdown file
└── VERSION                                    # Version identification text file

6 directories, 18 files
```
## HOW TO BUILD THIS APPLICATION
```Shell
$ cd example_of_loading_modules_on_demand
$ make clean all
```
## SOFTWARE REQUIREMENTS
- For usage, nothing particular...
- Developped and tested on XUBUNTU 22.10, GCC version 12.2.0  (Ubuntu 12.2.0-3ubuntu1), GNU Make 4.3

## RELEASE NOTES
Refer to file [RELEASENOTES](./RELEASENOTES.md).

***