# RELEASE NOTES: *example_of_loading_modules_on_demand*, Example of loading modules on demand.

Functional limitations, if any, of this version are described in the *README.md* file.

**Version 2.0.0**:
  - One library per function, instead of one global library.

**Version 1.0.0**:
  - First version.
