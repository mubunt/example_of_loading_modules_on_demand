//------------------------------------------------------------------------------
// Copyright (c) 2022, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: example_of_loading_modules_on_demand
// Example of loading modules on demand
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM HEADER FILES
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdarg.h>
#include <stdbool.h>
#include <limits.h>
#include <libgen.h>
#include <dlfcn.h>
//------------------------------------------------------------------------------
// MACROS
//------------------------------------------------------------------------------
#define error( ... )			_output(0, __VA_ARGS__)
#define information( ... )		_output(1, __VA_ARGS__)
//------------------------------------------------------------------------------
// LOCAL FUNCTIONS
//------------------------------------------------------------------------------
static void _output( short int type, const char *format, ... ) {
#define ESC 		0x1b
#define COLOR_TRAP	91	// BRIGHT RED
#define COLOR_INFO	93	// BRIGHT YELLOW
	va_list argp;
	char buff[512];
	va_start(argp, format);
	vsprintf(buff, format, argp);
	va_end(argp);
	switch (type) {
	case 0:
		fprintf(stderr, "%c[1;%dmERROR: %s. Abort!%c[0m\n\n", ESC, COLOR_TRAP, buff, ESC);
		break;
	case 1:
		fprintf(stderr, "%c[0;%dm%s%c[0m\n", ESC, COLOR_INFO, buff, ESC);
		break;
	default:
		break;
	}
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void freemodules( char **t, int n ) {
	for (int i = 0; i < n; i++) {
		if (t[i] == NULL) break;
		free(t[i]);
	}
	free(t);
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static char *getrealpath( char *e ) {
#define ProcessPseudoFileSystem		"/proc/self/exe"
	size_t n = PATH_MAX * sizeof(char);
	char *p = malloc(n);
	if (readlink(ProcessPseudoFileSystem, p, n) != -1) return p;
	free(p);
	return NULL;
}
//------------------------------------------------------------------------------
// MAIN
//------------------------------------------------------------------------------
// Note; for this demonstration purpose, we do not test the report of "malloc/free" functions.
// This is intentionally but must be done in a real application!!!

// About dynamically loaded library: https://tldp.org/HOWTO/Program-Library-HOWTO/dl-libraries.html

int main(int argc, char **argv) {
	if (argc == 1) {
		information("USAGE: %s [ acq ] [ dec ] [ col ] [ enc ] [ dis ]", argv[0]);
		return EXIT_FAILURE;
	}
	char **modules = malloc((size_t)(argc - 1) * sizeof(char *));
	int nbmodules = argc - 1;
	//---- Parameter checking and setting --------------------------------------
	for (int i = 1; i < argc; i++) {
		size_t j;
		modules[i -1] = NULL;
		if ((j = strlen(argv[i])) == 0) {
			error("The %d'nd argument is null", i);
			freemodules(modules, nbmodules);
			return EXIT_FAILURE;
		}
		modules[i -1] = malloc((j + 1) * sizeof(char));
		strcpy(modules[i - 1], argv[i]);
	}
	//----  Go on --------------------------------------------------------------
	// Get library path...
	char *execpath = getrealpath(argv[0]);
	if (execpath == NULL) {
		error("Cannot get path of the current executable.", argv[0]);
		freemodules(modules, nbmodules);
		free(execpath);
		return EXIT_FAILURE;
	}
	execpath = dirname(execpath);

	char *err;
	char *(*result)(void);
	for (int i = 0; i < nbmodules; i++) {
		// Build library name...
		char *lib = malloc((strlen(execpath) + strlen(modules[i]) + 8) * sizeof(char));
		sprintf(lib, "%s/lib%s.so", execpath, modules[i]);
		// Load library...
		information("Loading library %s", lib);
		void *handle = dlopen(lib, RTLD_LAZY);
		if (!handle) {
			error("%s", dlerror());
			freemodules(modules, nbmodules);
			free(execpath);
			free(lib);
			return EXIT_FAILURE;
		}
		// Execute modules...
		information("%s", "Executing entry point");
		result = dlsym(handle, modules[i]);
		if ((err = dlerror()) != NULL) {
			error("%s", dlerror());
			freemodules(modules, nbmodules);
			free(execpath);
			free(lib);
			return EXIT_FAILURE;
		}
		fprintf(stdout, "%s ... OK\n", (*result)());
		fflush(stdout);
		// Close library...
		information("Closing library %s", lib);
		dlclose(handle);
		// Free library name...
		free(lib);
	}
	
	// Free all alocations....
	free(execpath);
	freemodules(modules, nbmodules);
	//---- Exit ----------------------------------------------------------------
	return EXIT_SUCCESS;
}
//------------------------------------------------------------------------------
